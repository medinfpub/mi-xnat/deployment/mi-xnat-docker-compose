#!/bin/sh

cd 
envsubst < /usr/share/CTP/config.xml.tmpl > /usr/share/CTP/config.xml
envsubst < /usr/share/CTP/users.xml.tmpl > /usr/share/CTP/users.xml

#  Start CTP
mkdir -p /usr/share/CTP/logs/ 
touch  /usr/share/CTP/logs/ctp.log

cd /usr/share/CTP/
java -jar /usr/share/CTP/Runner.jar &
tail -n 1024 -f /usr/share/CTP/logs/ctp.log
