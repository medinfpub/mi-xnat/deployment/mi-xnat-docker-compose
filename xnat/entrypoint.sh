#!/bin/sh
# wait-for-postgres.sh

set -e

cmd="$@"

export PGPASSWORD=${XNAT_DATASOURCE_PASSWORD}

/usr/local/bin/make-xnat-config.sh
/usr/local/bin/wait-for-postgres.sh $cmd